"""
Models to create the tables in the database.
Contains 8 models:

1. **SubmissionBoxes** - SubmissionBoxes.
2. **User** - User.
3. **SubmissionAccounts** - SubmissionAccounts.
4. **Country** - Country.
5. **AccountStableIDs** - AccountStableIDs.
6. **DownloadBoxes** - DownloadBoxes.
7. **BoxHistory** - BoxHistory.
8. **EgaBoxNumbersAvailable** - EgaBoxNumbersAvailable.
"""

from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator

# === SubmissionBoxes ===
class SubmissionBoxes(models.Model):

    ega_box_number = models.PositiveIntegerField(primary_key=True, unique=True)
    account_type = models.CharField(
        max_length=30, blank=True, null=True)
    temporary_column = models.CharField(
        max_length=30, blank=True, null=True)
    account_opened_date = models.DateTimeField(
        default=timezone.now)
    password_changed_date = models.DateTimeField(blank=True, null=True)
    rt_ticket = models.CharField(max_length=30, unique=True)
    emails = models.CharField(max_length=300)
    comments = models.TextField(blank=True, null=True)
    received_dta = models.BooleanField(default=False)

    sent_dta = models.BooleanField(default=False)
    days_passed_since_created_date = models.PositiveIntegerField(
        blank=True, null=True)

    class Meta:
        db_table = 'submissionboxes'

    def __str__(self):
        return str(self.ega_box_number)

    def get_users(self):
        return ', '.join(self.users.all().values_list('email', flat=True))


# === User ===
class User(models.Model):
    email = models.EmailField(max_length=100)
    submissionboxes = models.ForeignKey(
        SubmissionBoxes,
        related_name='users', on_delete=models.CASCADE)

    class Meta:
        db_table = 'user'

    def __str__(self):
        return self.email


# === SubmissionAccounts ===
class SubmissionAccounts(models.Model):

    ega_box_number = models.PositiveIntegerField(
        unique=True,  primary_key=True)
    account_accession = models.CharField(max_length=50, unique=True)
    emails = models.CharField(max_length=300)
    submission_type = models.CharField(max_length=60, blank=True, null=True)
    country = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    institute_name = models.CharField(max_length=100)
    mapi = models.CharField(max_length=10, blank=True, null=True)
    affiliation_project = models.CharField(
        max_length=50, blank=True, null=True)
    center_name = models.CharField(max_length=80)
    password = models.CharField(max_length=80)
    comments = models.TextField(blank=True, null=True)
    submission_box = models.ForeignKey(
        SubmissionBoxes, on_delete=models.CASCADE)
    account_opened_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s %s %s' % (self.ega_box_number, self.account_accession, self.emails)

    def get_counthry(self):
        self.country = ', '.join(
            self.counthry.all().values_list('name', flat=True))
        return self.country

# === Country ===
class Country(models.Model):
    name = models.CharField(max_length=600)
    submissionAccounts_country = models.ForeignKey(
        SubmissionAccounts,
        related_name='counthry', on_delete=models.CASCADE)

    class Meta:
        db_table = 'country'

    def __str__(self):
        return self.name


# === AccountStableIDs ===
class AccountStableIDs(models.Model):
    ega_box_text = models.CharField(
        primary_key=True, max_length=100, unique=True)
    submission_boxes = models.OneToOneField(
        SubmissionBoxes,
        on_delete=models.CASCADE
    )

    submission_account = models.OneToOneField(
        SubmissionAccounts,
        on_delete=models.CASCADE,
        blank=True, null=True
    )

    account_accession = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return '%s %s' % (self.ega_box_text, self.account_accession)


# === DownloadBoxes ===
class DownloadBoxes(models.Model):

    box = models.PositiveIntegerField(primary_key=True)
    account_type = models.CharField(max_length=20)
    re_distribution = models.BooleanField(default=False)
    tempory_col = models.CharField(max_length=60)

    user = models.EmailField(max_length=70)
    account_opened_date = models.DateTimeField()

    password_changed_date = models.DateTimeField()
    expired = models.BooleanField(default=False)
    comments = models.TextField()

    def __str__(self):
        return '%s %s' % (self.box, self.user)

# === BoxHistory ===
class BoxHistory(models.Model):

    ega_name = models.CharField(max_length=100)
    user = models.EmailField(max_length=70)
    account_opened_date = models.DateTimeField()
    password_changed_date = models.DateTimeField(blank=True, null=True)
    comments = models.TextField()  
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s %s' % (self.ega_name, self.user)


# === EgaBoxNumbersAvailable ===
class EgaBoxNumbersAvailable(models.Model):
    id = models.AutoField(primary_key=True)
    ega_numbers_disponibles = models.CharField(
        max_length=1000, blank=True, null=True)
    ega_maxim = models.PositiveIntegerField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
