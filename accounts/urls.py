from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'accounts'

urlpatterns = [
    path('', views.show_home, name='show_home'),
    path('preasign', views.create_submissionboxes_with_users, name='create_submissionboxes_with_users'),
    path('stablesids/list', views.AccountStableIDsListView.as_view(), name='stablesidst_list'),
    path('all', views.list_accounts, name='list_accounts'),
    path('numbersavailable/list', views.EgaBoxNumbersAvailableListView.as_view(), name='numbersavailable_list'),
    path('history/list', views.BoxHistoryListView.as_view(), name='history_list'),
    path('numbersavailable/modify', views.change_number_available , name='change_number_available'),
    path('detail/<pk>/', views.SubmissionBoxesDetailView.as_view(), name='submissionboxes_detail'),
    path('account/<pk>/', views.SubmissionAccountDetailView.as_view(), name='account_detail'),
    path('detail/account/<pk>/', views.modify_submissionboxes, name='sub_detail'),
    path('create/account/<pk>/', views.create_account, name='create_account'),
    path('find', views.find_account , name='find_account'),
    path('deletesb/<pk>/', views.delete_sb, name='delete_sb'),
   
]
