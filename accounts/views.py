"""
Views where functions and templates are called.
Currently we support the following 13 functions:

1.  **show_home** - home page
2.  **SubmissionAccountDetailView** - Exists account 
3.  **SubmissionBoxesDetailView** - Request successful when create a pre-assign account
4.  **AccountStableIDsListView** - List AccountStableIDs
5.  **BoxHistoryListView** - List All History
6.  **list_accounts** - List All
7.  **EgaBoxNumbersAvailableListView** - List All Ega numbers available
8.  **change_number_available** - Modify Ega numbers available
9.  **delete_sb** - delete account
10. **create_submissionboxes_with_users** - Pre-assign account
11. **find_account** - Assign account
12. **create_account** - Create account
13. **modify_submissionboxes** - Modify Pre-assign account
"""

from django.shortcuts import render, redirect, reverse, get_list_or_404, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from django.views import generic
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from .forms import (
    SubmissionBoxesModelForm,
    UserFormset,
    FindSubmissionAccountForm,
    ModifySubBoxesModelForm,
    SubmissionAccountsModelForm,
    CountryFormset,
    EgaBoxNumbersAvailableModelForm
)
from .models import (
    SubmissionBoxes,
    User,
    SubmissionAccounts,
    Country,
    EgaBoxNumbersAvailable,
    AccountStableIDs,
    BoxHistory)
from django.core import serializers
import json

# === show_home ===
def show_home(request):
    template_name = 'accounts/home.html'
    heading = 'Submission account creation'
    return render(request, template_name, {
        'heading': heading
    })

# === SubmissionAccountDetailView ===
class SubmissionAccountDetailView(DetailView):
    model = SubmissionAccounts
    template_name = 'accounts/account_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['heading'] = 'Exists :=)'
        return context

# === SubmissionBoxesDetailView ===
class SubmissionBoxesDetailView(DetailView):
    model = SubmissionBoxes
    template_name = 'accounts/submission_boxes_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['heading'] = 'Request successful'
        return context


# === AccountStableIDsListView ===
class AccountStableIDsListView(generic.ListView):
    model = AccountStableIDs
    context_object_name = 'accountStableIDs'
    template_name = 'accounts/accountStableIDs_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['heading'] = 'List AccountStableIDs'
        return context

# === BoxHistoryListView ===
class BoxHistoryListView(generic.ListView):
    model = BoxHistory
    context_object_name = 'boxhistory'
    template_name = 'accounts/boxhistory_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['heading'] = 'List All History'
        return context

# === list_accounts ===
def list_accounts(request):
    submissionboxes = SubmissionBoxes.objects.all()
    accounts = SubmissionAccounts.objects.all()
    template_name = 'accounts/list.html'
    heading = 'List All'
    return render(request, template_name, {
        'submissionboxes': submissionboxes,
        'accounts': accounts,
        'heading': heading
    })

# === EgaBoxNumbersAvailableListView ===
class EgaBoxNumbersAvailableListView(generic.ListView):
    model = EgaBoxNumbersAvailable
    context_object_name = 'numbers'
    template_name = 'accounts/ega_numbers_available_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['heading'] = 'List All Ega numbers available'
        return context

        
# === change_number_available ===
def change_number_available(request):
    template_name = 'accounts/ega_number_available_change.html'
    heading = 'Modify Ega numbers available'
    em = ''
    end = ''
    eq = ''
    objects = EgaBoxNumbersAvailable.objects.all().count()
    if objects > 0:
        p = EgaBoxNumbersAvailable.objects.all().last()
        em = p.ega_maxim
        end = p.ega_numbers_disponibles
        eq = p.quantity

    if request.method == 'GET':
        form = EgaBoxNumbersAvailableModelForm(request.GET or None)
    elif request.method == 'POST':
        form = EgaBoxNumbersAvailableModelForm(request.POST)
        if form.is_valid():
            ega_maxim = form.cleaned_data['ega_maxim']
            #

            ega_numbers_uso = list(SubmissionBoxes.objects.all(
            ).values_list('ega_box_number', flat=True))
            ega_numbers_disponibles = []

            for i in range(1, int(ega_maxim)):
                if i not in ega_numbers_uso:
                    ega_numbers_disponibles.append(i)
            pstr = ";".join([str(num) for num in ega_numbers_disponibles])
            list_int = [int(num) for num in pstr.split(";")]
            quan = len(list_int)

            #

            post = form.save()
            post.ega_numbers_disponibles = pstr
            post.quantity = quan
            post.save()
            return redirect('accounts:numbersavailable_list')
    return render(request, template_name, {
        'form': form,
        'em': em,
        'end': end,
        'eq': eq,
        'heading': heading
    })


# === delete_sb ===
def delete_sb(request, pk):

    if request.method == 'POST':
        sb = SubmissionBoxes.objects.get(pk=pk)
        BoxHistory.objects.create(ega_name=sb.ega_box_number, user=sb.emails,
                                  account_opened_date=sb.account_opened_date, comments=sb.comments)

        sb.delete()

    return redirect('accounts:list_accounts')

# === create_submissionboxes_with_users ===
def create_submissionboxes_with_users(request):
    template_name = 'accounts/submission_boxes_new.html'
    heading = 'Pre-assign account'
    # get maxim of Numbers Available
    numbers_maxims = list(EgaBoxNumbersAvailable.objects.all(
    ).values_list('ega_maxim', flat=True))
    pstr = ''
    if len(numbers_maxims) > 0:
        maxi_num = max(numbers_maxims)
        #
        ega_numbers_uso = list(SubmissionBoxes.objects.all(
        ).values_list('ega_box_number', flat=True))
        ega_numbers_disponibles = []

        for i in range(1, int(maxi_num)):
            if i not in ega_numbers_uso:
                ega_numbers_disponibles.append(i)
        pstr = ";".join([str(num) for num in ega_numbers_disponibles])
        list_int = [int(num) for num in pstr.split(";")]
        quan = len(list_int)
        #
    if request.method == 'GET':
        submissionboxesform = SubmissionBoxesModelForm(request.GET or None)
        formset = UserFormset(queryset=User.objects.none())
    # POST
    elif request.method == 'POST':
        submissionboxesform = SubmissionBoxesModelForm(request.POST)
        # formset
        num_formset = int(''.join(request.POST['form-TOTAL_FORMS']))
        list_emails = []
        for i in range(0, num_formset):
            list_emails.append(request.POST['form-'+str(i)+'-email'])
        string_emails = ', '.join(list_emails)
        #
        formset = UserFormset(request.POST)
        if submissionboxesform.is_valid() and formset.is_valid():
            submissionboxes = submissionboxesform.save(commit=False)
            submissionboxes.emails = string_emails
            submissionboxes.save()
            ega_box_number = submissionboxesform.cleaned_data['ega_box_number']
            # LIST EGA BOX NUMBERS AVAILABLE
            if len(numbers_maxims) > 0:
                ega_numbers_uso = list(SubmissionBoxes.objects.all(
                ).values_list('ega_box_number', flat=True))
                ega_numbers_disponibles = []

                for i in range(1, int(maxi_num)):
                    if i not in ega_numbers_uso:
                        ega_numbers_disponibles.append(i)
                pstr = ";".join([str(num) for num in ega_numbers_disponibles])
                list_int = [int(num) for num in pstr.split(";")]
                quan = len(list_int)
                ega_a = EgaBoxNumbersAvailable.objects.create(
                    ega_numbers_disponibles=pstr, ega_maxim=int(maxi_num), quantity=quan)
                #
            for form in formset:
                user = form.save(commit=False)
                user.submissionboxes = submissionboxes
                user.save()
            return redirect('accounts:submissionboxes_detail', ega_box_number)

    return render(request, template_name, {
        'submissionboxesform': submissionboxesform,
        'formset': formset,
        'pstr': pstr,
        'heading': heading
    })

# === find_account ===
def find_account(request):
    form = FindSubmissionAccountForm()
    heading = 'Assign account'
    mes_exists = ""
    if request.method == "POST":
        form = FindSubmissionAccountForm(request.POST)
        if form.is_valid():
            print("moamaoamao")
            ega_box_number = request.POST['ega_box']
            # exists submission boxes
            exists_sb = SubmissionBoxes.objects.filter(
                ega_box_number=ega_box_number).count()
            if exists_sb == 1:
                return redirect('accounts:sub_detail', pk=ega_box_number)
            else:
                mes_exists = "This ega box number does not exist. Not pre-assigned."
    return render(request, 'accounts/account_find.html', {
        'form': form,
        'mes_exists': mes_exists,
        'heading': heading
    })


# === create_account ===
def create_account(request, pk):
    post = get_object_or_404(SubmissionBoxes, pk=pk)
    template_name = 'accounts/account_create.html'
    heading = 'Create account'
    #
    gebt = "ega-box-"+str(pk)
    get_accession = AccountStableIDs.objects.get(ega_box_text=gebt)
    account_accession = get_accession.account_accession
    #
    if request.method == 'GET':
        form = SubmissionAccountsModelForm(instance=post)
        formset = CountryFormset(queryset=Country.objects.none())
    elif request.method == "POST":
        form = SubmissionAccountsModelForm(request.POST)
        # formset
        num_formset = int(''.join(request.POST['form-TOTAL_FORMS']))
        list_countries = []
        for i in range(0, num_formset):
            list_countries.append(request.POST['form-'+str(i)+'-name'])
        string_countries = ', '.join(list_countries)
        #
        formset = CountryFormset(request.POST)
        if form.is_valid() and formset.is_valid():
            ega_box_number = request.POST['ega_box_number']

            submission_type_string = ', '.join(
                form.cleaned_data['submission_type'])

            post = form.save(commit=False)
            post.submission_type = submission_type_string
            post.country = string_countries
            post.submission_box = SubmissionBoxes.objects.get(
                ega_box_number=ega_box_number)
            post.save()
            get_accession.submission_account = post
            get_accession.save()

            for form in formset:
                country = form.save(commit=False)
                country.submissionAccounts_country = post
                country.save()

            return redirect('accounts:list_accounts')

    return render(request, template_name, {
        'form': form,
        'account_accession': account_accession,
        'formset': formset,
        'heading': heading
    })

# === modify_submissionboxes ===
def modify_submissionboxes(request, pk):
    post = get_object_or_404(SubmissionBoxes, pk=pk)
    template_name = 'accounts/subBoxacount_detail.html'
    heading = 'Modify Pre-assign account'
    date = post.account_opened_date
    if request.method == "POST":
        received_dta = request.POST.get('received_dta', '')
        form = ModifySubBoxesModelForm(request.POST, instance=post)
        if form.is_valid():

            ega_box_number = request.POST['ega_box_number']
            exists_account = SubmissionAccounts.objects.all().filter(
                ega_box_number=ega_box_number).exists()
            if received_dta == "on":
                if exists_account == False:
                    #
                    sb = SubmissionBoxes.objects.get(
                        ega_box_number=ega_box_number)
                    ebt = "ega-box-"+str(sb.ega_box_number)
                    if AccountStableIDs.objects.all().count() == 0:
                        ac = "EGAB"+'1'.zfill(11)
                        create_accession = AccountStableIDs.objects.create(
                            ega_box_text=ebt, submission_boxes=sb, account_accession=ac)
                    else:  #
                        list_acs = list(AccountStableIDs.objects.all(
                        ).values_list('account_accession', flat=True))
                        maxi_ac = max(list_acs)
                        last = AccountStableIDs.objects.get(
                            account_accession=maxi_ac)
                        l_ac = last.account_accession
                        split_l_ac = l_ac.split('EGAB')
                        num_ac = int(split_l_ac[1])+1
                        ac = "EGAB" + str(num_ac).zfill(11)

                        exists_asid = AccountStableIDs.objects.filter(
                            ega_box_text=ebt).count()

                        if exists_asid == 1:
                            AccountStableIDs.objects.all().filter(ega_box_text=ebt).delete()

                        create_accession = AccountStableIDs.objects.create(
                            ega_box_text=ebt, submission_boxes=sb, account_accession=ac)
                    #
                    post = form.save()
                    post.save()
                    return redirect('accounts:create_account', pk=post.pk)

                else:
                    return redirect('accounts:account_detail', pk=post.pk)
            else:
                delete_check = SubmissionAccounts.objects.all().filter(
                    ega_box_number=ega_box_number).delete()
                delete_ac = AccountStableIDs.objects.all().filter(
                    ega_box_text="ega-box-"+str(ega_box_number)).delete()
                post = form.save()
                post.save()
                return redirect('accounts:submissionboxes_list')

    else:
        form = ModifySubBoxesModelForm(instance=post)
    return render(request, template_name, {
        'form': form,
        'date': date,
        'heading': heading
    })
